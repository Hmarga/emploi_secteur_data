import os
import time
import create_database
import api_pe
import scrap_monster as sm
import rss_indeed
import fusion_raw as fr
import tableau as tab
import call_scrap_competences as csc
import merge_find_competences as mfc

#Création d'un dossier pour stocker les csv s'il n'existe pas déjà.
if not os.path.exists('../csv'):
    os.mkdir('../csv')

#Collecte des données
df_pe =api_pe.api_pe("data", "données")
print("api_pe OK ")

url = sm.recup_nb_page("data", "données")
df_monster = sm.scrap_monster(url)
print("scrap_monster OK")

df_indeed = rss_indeed.rss_indeed()
print("rss_indeed OK ")

#Fusion des données
fr.fusion_raw()
fr.aurevoir_doublons()
print("fusion_csv OK ")

CMD = 'scrapy runspider scrapy_competences.py --logfile log.txt'
csc.run_bash(CMD)
time.sleep(5)
print("fait scrapy_competences")

mfc.ranking_competences('p')
print("fait ranking_merged")

tab.get_tableau()
print("creation liste.html OK")

create_database.create_database()
print("create_database OK ")
