import csv
import pandas as pd
import scrapy
from bs4 import BeautifulSoup

class MPS(scrapy.Spider):
    '''
    File to be called form bash as ----> scrapy runspider scrapy_competences.py --logfile log.txt
    '''
    name = "project"
    allowed_domains = ["monster.fr"]

    def start_requests(self):
        fusion_sans_doublons = pd.read_csv('../csv/fusion_sans_doublons.csv')
        fsd = fusion_sans_doublons[fusion_sans_doublons['lien'].str.contains('monster')].sort_values(by='date', ascending=False)
        fsd = fsd['lien'].str.strip()
        urls = fsd.values.tolist()

        for url in urls:
            yield scrapy.Request(url=url,
                                callback=self.parse)#,
                                #dont_filter = True) # This parameter was filtering requests

    def parse(self, response):
        soup = BeautifulSoup(response.text, 'html.parser')
        with open('../csv/fusion_comp.csv', 'a', newline='') as file:
            fieldnames = ['url','description']

            writer = csv.DictWriter(file, fieldnames=fieldnames)
            a = soup.find_all("div", class_= "job-description")
            if len(a) > 0:
                page = a[0].text.split(',')
            else:
                page = ["Website not available..."]

            writer.writerow({'url': response.url, 'description': page})
