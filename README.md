# Job Dashboard

## Pré-requis
docker  
postgres  
pipenv   

## Lancement du pipeline
```bash
bash pipeline.sh
```
En lançant le pipeline, les serveurs postgres et metabase sont initialisés ainsi que l'environement virtuel qui installera les librairies suivant :  
-   pandas
-   bs4 
-   jupyter 
-   requests
-   seaborn 
-   psycopg2
-   matplotlib
-   folium 
-   api-offres-emploi
-   feedparser 

Suite à l'installation des librairies, un script python nommé main.py sera lancé, il fera appel à des fonctions via des modules que nous avons coder.  
Nos modules :
-   récupére des offres d'emploi lié à la data :
    -   par api pour le site pole emploi
    -   par scraping sur le site monster
    -   par flux rss pour le site indeed
-   crée un csv à partir de dataframe des 3 methodes de récuperation d'offre d'emploi
-   crée une catégorisation de metier à partir des intitulés des offres
-   retirer les doublons   
-   crée une cartographie  
-   crée des graphique  
-   crée un systeme de matching par rapport aux compétences de la formation avec les compétences demandées dans l'offre d'emploi

<img src="/images/Pipeline.png" alt="Pipeline">

## Gitlab pages
https://simplon-dev-data.gitlab.io/grenoble-2020-2021/projets/projet_3/projet_3-groupe_2/  
Ce lien vous emmène sur "notre" site crée à partir de gitlab pages.  
Vous y trouverez notre tableau des offres de jobs, une representation cartographique des offres d'emploi par type de poste par ville et des graphiques montrant les tendances de metiers à partir des publications des offres d'emploi par semaine. Un bouton pour télécharger le csv est mis à disposition.

## Bibliothèque 
### Présentation
-   https://docs.google.com/presentation/d/1zmnO1QrJvFO0LoBzjFQgOiteYOqD0huTHF-vL5-MDWg/edit?usp=sharing  
### Site utilisé pour notre dashboard
-   Pole-emploi : https://www.pole-emploi.fr/accueil/
-   Monster : https://www.monster.fr/
-   Indeed : https://fr.indeed.com/

### Librairie python 
-   Pandas : https://pandas.pydata.org/
-   Mathplotlib : https://matplotlib.org/stable/index.html
-   BeautifulSoup : https://www.crummy.com/software/BeautifulSoup/bs4/doc/
-   Folium : https://python-visualization.github.io/folium/
-   API Pole-emploi : https://pypi.org/project/api-offres-emploi/
-   Feedparser : https://pythonhosted.org/feedparser/

